#include <stdio.h>

main()
{
    float fahr, celsius;
    int lower, upper, step;

    lower = 0;
    upper = 300;
    step = 20;

    fahr = lower;
    while (fahr <= upper) {
        // celsius = 5 * (fahr-32) / 9;

        celsius = 5.0 / 9.0 * (fahr-32); 
        // here I try using a floating point
        // for the constants so that it looks
        // tidier (imo)

        printf("%3.0f %7.2f\n", fahr, celsius);
        fahr = fahr + step;
    }
}
