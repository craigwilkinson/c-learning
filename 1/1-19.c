#include <stdio.h>

#define MAXLEN 1000

void reverse(char line[], int len);
int mygetline(char line[], int max);

main()
{
    char line[MAXLEN];
    int i, len;
    
    while ((len = mygetline(line, MAXLEN)) > 0) {
        reverse(line, len);
        printf("%s", line);
    }

}

/* Take a line of characters and reverse it
   in place */
void reverse(char line[], int len) {
    int i;
    char result[len];
    for (i = len - 2; i >= 0; i--) // start 2 from the
                                   // end because of newlines
        result[len-i-2] = line[i];

    result[len - 1] = '\n';
    result[len] = '\0';

    for (i = 0; i < len; i++)
        line[i] = result[i];

    line[i] = '\0';
}

int mygetline(char line[], int max) {
    int i, c;
    for (i = 0; i < MAXLEN && (c = getchar()) != EOF && c != '\n'; i++)
        line[i] = c;

    if (c == '\n') {
        line[i] = c;
        i++;
    }

    line[i] = '\0';

    return i;
}
