#include <stdio.h>

main()
{
    int ndigits[10];
    int i, c;

    for (i = 0; i < 10; i++)
        ndigits[i] = 0;

    while ((c = getchar()) != EOF) {
        if (c >= '0' && c <= '9')
            ndigits[c-'0']++;
    }

    for (i = 0; i < 10; i++)
        printf("%d ", ndigits[i]);

    printf("\n");
}
