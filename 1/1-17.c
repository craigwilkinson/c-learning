#include <stdio.h>

#define MAXLEN 1000
#define THRESH 50

/* get a line from the standard input
   returns 0 if the line is EOF */
int mygetline(char line[], int lim);


/* This program takes input and only prints
   lines if they are longer than the threshold
   amount of characters */

main()
{
    char line[MAXLEN];
    int length;
    while ((length = mygetline(line, MAXLEN)) > 0) {
        if (length > THRESH)
            printf("%d: %s\n", length, line);
    }
}

int mygetline(char line[], int lim) {
    int c, i;

    // continue taking characters until we find a newline
    // or an end of file, or we reach the limit of characters
    // here we need to use lim-1 so there is room to put the
    // null terminator in there
    for (i = 0; i < lim-1 && (c = getchar()) != EOF && c != '\n'; i++)
        line[i] = c;

    if (c == '\n') {
        line[i] = c;
        i++;
    }

    line[i] = '\0'; // terminate the string

    return i;
}
