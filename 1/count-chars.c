#include <stdio.h>

/* Just counts the number of characters in a program */
main()
{
    int nc = 0;
    while (getchar() != EOF)
        ++nc;

    printf("%d\n", nc);
}
