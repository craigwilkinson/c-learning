#include <stdio.h>

#define MAXLEN 1000

int mygetline(char line[], int max);
void stripline(char line[], int len);

/* Remove trailing tabs and spaces from lines
   Unlike the question, I don't remove empty lines
   because that doesn't seem as useful to me */
main()
{
    char line[MAXLEN];
    int length;

    while ((length = mygetline(line, MAXLEN)) > 0) {
        stripline(line, length);
        printf("%s", line);
    }

    return 0;
}

int mygetline(char line[], int max) {
    int c, i;

    for (i = 0; i < max-1 && (c = getchar()) != EOF && c != '\n'; i++)
        line[i] = c;

    if (c == '\n') {
        line[i] = '\n';
        i++;
    }

    line[i] = '\0';

    return i;
}

void stripline(char line[], int length) {
    int i;
    for (i = length - 2; i >= 0 && (line[i] == ' ' || line[i] == '\t'); i--) {
        line[i] = '\n';
    }

    line[i+2] = '\0';
}
