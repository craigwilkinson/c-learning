#include <stdio.h>

// use some for loop magic to tidy it all up a little
main()
{
    printf("Fahrenheit\tCelsius\n");
    float fahr;
    for (fahr = 300; fahr >= 0; fahr -= 20)
        printf("%3.0f\t\t%6.3f\n", fahr, (5.0/9.0) * (fahr-32.0));

}
