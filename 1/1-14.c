#include <stdio.h>

// counts different character occurences
main()
{
    int uppercount[26];
    int lowercount[26];
    int digitcount[10];
    int spacecount, othercount, i, j, c;

    spacecount = othercount = 0;

    for (i = 0; i < 26; i++)
        uppercount[i] = lowercount[i] = 0;

    for (i = 0; i < 10; i++)
        digitcount[i] = 0;

    while ((c = getchar()) != EOF) {
        if (c >= 'a' && c <= 'z')
            lowercount[c - 'a']++;
        else if (c >= 'A' && c <= 'Z')
            uppercount[c - 'A']++;
        else if (c >= '0' && c <= '9')
            digitcount[c - '0']++;
        else if (c == ' ')
            spacecount++;
        else
            othercount++;
    }

    // for each letter, lower and upper
    for (i = 0; i < 26; i++) {
        printf("%c ", 'a' + i);
        for (j = 0; j < lowercount[i]; j++)
            putchar('#'); 

        printf("\n%c ", 'A' + i);
        for (j = 0; j < uppercount[i]; j++)
            putchar('#'); 

        putchar('\n');
    }

    // print out the digit occurences
    for (i = 0; i < 10; i++) {
        printf("%c ", '0' + i);
        for (j = 0; j < digitcount[i]; j++)
            putchar('#');

        putchar('\n');
    }

    printf("space: ");
    for (i = 0; i < spacecount; i++)
        putchar('#');

    printf("\nother: ");
    for (i = 0; i < othercount; i++)
        putchar('#');


    putchar('\n');
}
