#include <stdio.h>

// prints 
int power(int x, int y);

main()
{
    int i;
    for (i = 0; i < 20; i++)
        printf("2 ^ %d = %d\n", i, power(2,i));
}

int power(int x, int y) {
    int i, result;
    result = 1;
    for (i = 0; i < y; i++)
        result = result * x;

    return result;
}
