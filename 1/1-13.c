#include <stdio.h>

#define IN  1
#define OUT 0

// this program displays a histogram of the length
// of each word
main()
{
    int c, count;

    int word = OUT;

    count = 0;
    while ((c = getchar()) != EOF) {
        if (c == '\n' | c == ' ' | c == '\t') {
            word = OUT;
            if (count > 0) {
                int i;
                for (i = 0; i < count; i++)
                    putchar('#');

                putchar('\n');
                count = 0; // we reset the counter
            }
        } else {
            word = IN;
            count++;
        }
    }

}
