#include <stdio.h>

// program to convert celsius to fahrenheit
main()
{
    // fahr = (9/5 * cels) + 32 
    float fahr, cels;
    int lower, upper, step;

    lower = -50;
    upper = 50;
    step = 10;

    cels = lower;
    while (cels <= upper) {
        fahr = (9.0/5.0 * cels) + 32.0;
        printf("%3.0f %3.3f\n", cels, fahr);

        cels = cels + step;
    }
}
